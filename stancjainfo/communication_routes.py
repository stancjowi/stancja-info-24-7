from datetime import datetime

from flask import render_template, Blueprint, request, url_for, redirect
from flask_login import current_user, login_required

from stancjainfo import socketio, message_service

communication_module = Blueprint('communication_module', __name__)
timeFormat = "%Y-%m-%d %H:%M"


@communication_module.route('/chat')
@login_required
def chat():
    messages = message_service.get_by_namespace_limit('/main-chat', 50)
    messages.reverse()

    for message in messages:
        message.timestamp = message.timestamp.strftime(timeFormat)

    return render_template('main_chat.html', messages=messages)


@socketio.on('user connected', namespace='/main-chat')
def handle_user_connected_event(json):
    print('received', str(json))
    json['username'] = current_user.username
    socketio.emit('user connected response', json, broadcast=True, namespace='/main-chat')


@socketio.on('user disconnected', namespace='/main-chat')
def handle_user_disconnected_event(json):
    print('received', str(json))
    json['username'] = current_user.username
    socketio.emit('user disconnected response', json, broadcast=True, namespace='/main-chat')


@socketio.on('new message', namespace='/main-chat')
def handle_new_message_event(json):
    message = json['message']

    print('received', str(message))

    now = datetime.now()
    username = current_user.username

    json['username'] = username
    json['date'] = now.strftime("%Y-%m-%d %H:%M")

    message_service.create_message(username=username, message=message, namespace='/main-chat', room="", date=now)

    socketio.emit('new message published', json, broadcast=True, namespace='/main-chat')
    return redirect(url_for('communication_module.chat'))


@communication_module.route('/main-forum', methods=['POST'])
def handle_new_message():
    message = request.form['message']

    print('received', str(message))

    now = datetime.now()
    username = current_user.username
    avatar_url = current_user.avatar_m

    message_service.create_message(username=username, message=message, namespace='/main-chat', room="", date=now, avatar_url=avatar_url)
    return redirect(url_for('communication_module.chat'))
