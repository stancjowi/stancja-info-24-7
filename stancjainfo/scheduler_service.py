from stancjainfo.scheduler.scheduler import Scheduler

def init_scheduler_test():
    print("init sched")
    s = Scheduler()
    # s.add_job(MeasureSmog())
    # s.add_job(MeasureTemperatureHumidityPressure())
    s.init_jobs()
    s.run_continuously()
    print("done")