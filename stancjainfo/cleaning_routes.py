from flask import render_template, Blueprint, redirect, url_for
from flask_login import login_required

from stancjainfo import cleaning_service, definition_service

cleaning_module = Blueprint('cleaning_module', __name__)


@cleaning_module.route('/cleaning')
@login_required
def cleaning():
    bathroom_cleaner = cleaning_service.get_bathroom_cleaner_user()
    other_cleaner = cleaning_service.get_kitchen_other_cleaner()
    worktops_cleaner = cleaning_service.get_kitchen_worktops_cleaner()
    restroom_cleaner = cleaning_service.get_restroom_cleaner()
    floors_cleaner = cleaning_service.get_floors_cleaner()
    last_cleaning_scheduled_date = definition_service.get_by_key(definition_service.__last_cleaning_schedule_set_date)

    is_bathroom_cleaned = cleaning_service.is_bathroom_cleaned_str()
    is_kitchen_other_cleaned = cleaning_service.is_kitchen_other_cleaned_str()
    is_worktops_cleaned = cleaning_service.is_kitchen_worktops_cleaned_str()
    is_restroom_cleaned = cleaning_service.is_restroom_cleaned_str()
    is_floor_cleaned = cleaning_service.is_floor_cleaned_str()

    return render_template('cleaners.html',
                           bathroom_cleaner=bathroom_cleaner,
                           other_cleaner=other_cleaner,
                           worktops_cleaner=worktops_cleaner,
                           restroom_cleaner=restroom_cleaner,
                           floors_cleaner=floors_cleaner,
                           date=last_cleaning_scheduled_date,
                           is_bathroom_cleaned=is_bathroom_cleaned,
                           is_kitchen_other_cleaned=is_kitchen_other_cleaned,
                           is_worktops_cleaned=is_worktops_cleaned,
                           is_restroom_cleaned=is_restroom_cleaned,
                           is_floor_cleaned=is_floor_cleaned
                           )


@cleaning_module.route('/cleaning/cleaned/<cleaned_place>', methods=['POST'])
@login_required
def place_was_cleaned(cleaned_place):
    cleaning_service.mark_place_as_cleaned(cleaned_place)
    return redirect(url_for('cleaning_module.cleaning'))
