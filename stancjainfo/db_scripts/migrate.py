#!/usr/bin/python

import sqlite3
import sys
from sqlite3 import Error


def main():
    if sys.argv[0] is None or sys.argv[1] is None:
        raise Exception("Wrong arguments passed")

    database = sys.argv[1]
    sql_file = sys.argv[2]

    print('database:', database)
    print('file:', sql_file)

    statement = open(sql_file, 'r').read()
    print(statement)
    sqlite3.complete_statement(statement)
    conn = sqlite3.connect(database)
    cur = conn.cursor()
    try:
        cur.executescript(statement)
    except Error as e:
        print(e)


if __name__ == '__main__':
    main()