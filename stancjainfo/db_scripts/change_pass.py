#!/usr/bin/python

import sqlite3
import sys
from sqlite3 import Error

# $2b$12$gYdFCUC3ZIKU0v01crrCJOnfruGm0doOUSpVpy4rip14/fwQzyfsi

def main():
    if sys.argv[0] is None or sys.argv[1] is None or sys.argv[2] is None or sys.argv[3] is None:
        raise Exception("Wrong arguments passed")

    print('file:', sys.argv[0])
    print('database:', sys.argv[1])
    print('pass:', sys.argv[2])
    print('username:', sys.argv[3])
    database = sys.argv[1]
    hashed_password = sys.argv[2]
    username = sys.argv[3]
    sql_file = open('./other/change_pass_query.txt', 'r').read()

    print('database:', database)

    statement = sql_file % (hashed_password, username, username)
    print(statement)
    sqlite3.complete_statement(statement)
    conn = sqlite3.connect(database)
    cur = conn.cursor()
    try:
        cur.executescript(statement)
    except Error as e:
        print(e)


if __name__ == '__main__':
    main()