#!/usr/bin/python

import csv
import sqlite3
import sys
from sqlite3 import Error


def main():
    if sys.argv[0] is None or sys.argv[1] is None:
        raise Exception("Wrong arguments passed")

    database = sys.argv[1]
    csv_path = sys.argv[2]

    print('database:', database)
    print('csv_path:', csv_path)

    statement = "SELECT * FROM smog_measurement"
    print(statement)
    sqlite3.complete_statement(statement)
    conn = sqlite3.connect(database)
    cur = conn.cursor()
    try:
        cur.execute(statement)
        rows = cur.fetchall()
        with open(csv_path, 'w') as out:
            csv_out = csv.writer(out, delimiter=';')
            for row in rows:
                csv_out.writerow(row)
    except Error as e:
        print(e)


if __name__ == '__main__':
    main()