from typing import List, Tuple, Dict

from flask import flash
from flask_avatars import Identicon
from flask_login import current_user
from wtforms import ValidationError

from stancjainfo import db, definition_service
from stancjainfo.forms import UpdateUserForm
from stancjainfo.models import User, UserGroup


def get_by_id(entry_id: int) -> User:
    return User.query.get_or_404(entry_id)


def get_by_id_in(user_ids: List[int]):
    return User.query.filter(User.id.in_(user_ids)).all()


def activate_user(user_id: int):
    user = User.query.get_or_404(user_id)
    user.active = not user.active
    db.session.commit()


def accept_user(user_id: int):
    user = User.query.get_or_404(user_id)
    user.accepted = not user.accepted
    db.session.commit()


def get_all() -> List[User]:
    return User.query.all()


def update_user(user: User, form: UpdateUserForm):
    existing_user = User.query.filter_by(username=str(form.username.data).lower()).first()
    if existing_user and existing_user is not user:
        flash("Użytkownik z podaną nazwą użytkownika już istnieje", 'error')
        raise ValidationError('User with this username already exists')

    existing_user = User.query.filter_by(email=str(form.email.data).lower()).first()
    if existing_user and existing_user is not user:
        flash("Użytkownik z podanym email już istnieje", 'error')
        raise ValidationError('User with this email already exists')
    user.username = form.username.data
    user.name = form.name.data
    user.surname = form.surname.data
    user.email = form.email.data
    db.session.commit()


def fill_update_form(user: User, form: UpdateUserForm) -> UpdateUserForm:
    form.username.data = user.username
    form.name.data = user.name
    form.surname.data = user.surname
    form.email.data = user.email
    return form


def delete_by_id(entry_id: int):
    user = User.query.get_or_404(entry_id)
    db.session.delete(user)
    db.session.commit()


def get_internet_debtors_list() -> List[str]:
    definition = definition_service.get_by_key(definition_service.__debtors_key)
    if not definition:
        print('debtors definition not defined!')
        return []

    user_group = UserGroup.query.filter_by(name=definition.value).first()
    return ['{} {}'.format(user.name, user.surname) for user in user_group.users]


def get_internet_debtors_emails() -> List[str]:
    definition = definition_service.get_by_key(definition_service.__debtors_key)
    if not definition:
        print('debtors definition not defined!')
        return []

    user_group = UserGroup.query.filter_by(name=definition.value).first()
    return [user.email for user in user_group.users]


def get_media_payers_count() -> int:
    definition = definition_service.get_by_key(definition_service.__media_payers_key)
    if not definition:
        print('media payers definition not defined!')
        return 0

    user_group = UserGroup.query.filter_by(name=definition.value).first()
    return len(user_group)


def get_locator_debtors() -> List[User]:
    definition = definition_service.get_by_key(definition_service.__locator_debts_key)
    if not definition:
        print('locator debtors definition not defined!')
        return []

    user_group = UserGroup.query.filter_by(name=definition.value).first()
    return user_group


def get_locator_debtors_dto() -> List[Tuple]:
    definition = definition_service.get_by_key(definition_service.__locator_debts_key)
    if not definition:
        print('locator debtors definition not defined!')
        return []

    user_group = UserGroup.query.filter_by(name=definition.value).first()
    return [(user.id, '{} {}'.format(user.name, user.surname)) for user in user_group.users]


def tuple_list_to_user_map(users: List) -> Dict:
    users_dict = dict()
    for user in users:
        users_dict[user[0]] = user[1]
    return users_dict


def get_all_active_users_emails():
    active_users = User.query.filter_by(accepted=True, active=True).all()
    return [user.email for user in active_users]


def generate_avatars(user: User):
    avatar = Identicon()
    filenames = avatar.generate(user.username)
    user.avatar_s = filenames[0]
    user.avatar_m = filenames[1]
    user.avatar_l = filenames[2]
    db.session.commit()


def add_avatars_to_user(user: User, filenames):
    user.avatar_s = filenames[0]
    user.avatar_m = filenames[1]
    user.avatar_l = filenames[2]
    db.session.commit()


def get_current_user():
    return get_by_id(current_user.id)
