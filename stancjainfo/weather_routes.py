from flask import render_template, Blueprint
from flask_login import login_required

weather_module = Blueprint('weather_module', __name__)

@weather_module.route('/weather')
@login_required
def weather():
    return render_template('weather_main.html')


# @weather_module.route('/weather')
# @login_required
# def weather():
#     smog_data = smog_service.get_latest()
#     weather_data = bme280_service.get_latest()
#
#     if weather_data:
#         weather_data.temperature = weather_data.temperature - 3
#         weather_data.timestamp = weather_data.timestamp.strftime(timeFormat)
#
#     if smog_data:
#         smog_data.timestamp = smog_data.timestamp.strftime(timeFormat)
#
#     pm_25 = smog_data.pm25
#     if pm_25 < 12:
#         pm_25_color = 'very-good.png'
#     elif pm_25 < 25:
#         pm_25_color = 'good.png'
#     elif pm_25 < 36:
#         pm_25_color = 'ok.jpeg'
#     elif pm_25 < 60:
#         pm_25_color = 'sufficient.png'
#     elif pm_25 < 76:
#         pm_25_color = 'little-bad.png'
#     elif pm_25 < 84:
#         pm_25_color = 'bad.jpeg'
#     elif pm_25 < 120:
#         pm_25_color = 'very-bad.png'
#     else:
#         pm_25_color = 'disasterous.jpg'
#
#     pm_10 = smog_data.pm10
#     if pm_10 < 20:
#         pm_10_color = 'very-good.png'
#     elif pm_10 < 35:
#         pm_10_color = 'good.png'
#     elif pm_10 < 60:
#         pm_10_color = 'ok.jpeg'
#     elif pm_10 < 100:
#         pm_10_color = 'sufficient.png'
#     elif pm_10 < 130:
#         pm_10_color = 'little-bad.png'
#     elif pm_10 < 140:
#         pm_10_color = 'bad.jpeg'
#     elif pm_10 < 200:
#         pm_10_color = 'very-bad.png'
#     else:
#         pm_10_color = 'disasterous.jpg'
#
#
#     return render_template('weather_main.html', smog_data=smog_data, weather_data=weather_data, pm_10_color=pm_10_color, pm_25_color=pm_25_color)
