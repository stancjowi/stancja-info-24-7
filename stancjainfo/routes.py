import random
import string
from datetime import datetime

from flask import render_template, flash, redirect, url_for, request
from flask_login import login_user, current_user, logout_user, login_required

from stancjainfo import app, db, bcrypt, user_service, mail_service, constants
from stancjainfo.forms import RegistrationForm, LoginForm, ResetPasswordForm, NewPasswordForm
from stancjainfo.models import User


@app.route('/')
def hello_world():
    debtors = user_service.get_internet_debtors_list()
    return render_template('index.html', debtors=debtors)


@app.route('/whats-new')
@login_required
def whats_new():
    return render_template('whats_new.html')


def is_current_user_an_admin():
    return current_user.is_authenticated and User.query.get(current_user.id).role == 'ADMIN'


# noinspection PyArgumentList
@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('hello_world'))
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(username=str(form.username.data).lower(),
                    name=str(form.name.data).capitalize(),
                    surname=str(form.surname.data).capitalize(),
                    email=str(form.email.data).lower(),
                    password=hashed_password,
                    accepted=False,
                    active=True,
                    role='USER')
        db.session.add(user)
        db.session.commit()
        flash(f'Stworzono nowe konto {form.username.data}. Proszę czekać na akceptację', 'success')
        return redirect(url_for('hello_world'))

    return render_template('register.html', form=form)


@app.route('/login',  methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('hello_world'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=str(form.username.data).lower()).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            if not user.active:
                return render_template('user_inactive.html')
            login_user(user, remember=form.remember.data)
            user.last_logged = datetime.now()
            db.session.commit()
            next_page = request.args.get('next')
            flash('Zostałeś zalogowany', 'success')
            return redirect(next_page) if next_page else redirect(url_for('hello_world'))
        else:
            flash('Logowanie nieudane - błędne dane', 'error')

    return render_template('login.html', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('hello_world'))


@app.route('/password_reset', methods=['GET', 'POST'])
def forgot_password():
    if current_user.is_authenticated:
        return redirect(url_for('hello_world'))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=str(form.email.data)).first()
        print(str(form.email.data))
        if not user.active:
            return render_template('user_inactive.html')

        key = random_string()
        print(constants.RESET_PASSWORD_BODY + key)
        user.reset_key = key
        user.key_active = True
        db.session.commit()
        mail_service.send_to_user(constants.RESET_PASSWORD_SUBJECT, constants.RESET_PASSWORD_BODY + key, user.email)
        flash('Wysłano mail z linkiem do zmiany hasła', 'success')
        return redirect(url_for('hello_world'))

    return render_template('reset_password.html', form=form)


@app.route('/new_password', methods=['GET', 'POST'])
def new_password():
    form = NewPasswordForm()
    key = request.args.get('key')
    print(key)
    if form.validate_on_submit():
        user = User.query.filter_by(reset_key=key, key_active=True).first()
        if not user:
            flash('Błędny lub przestarzały link do zmiany hasła', 'error')
            return redirect(url_for('hello_world'))

        user.key_active = False
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user.password = hashed_password
        db.session.commit()
        flash('Hasło zostało zmnienione', 'success')
        return redirect(url_for('hello_world'))

    return render_template('new_password.html', form=form, key=key)


def random_string(stringLength=10):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))