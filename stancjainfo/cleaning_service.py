import random
from datetime import datetime
from enum import Enum
from typing import List

from stancjainfo import db, definition_service, group_service
from stancjainfo import user_service
from stancjainfo.forms import UserGroupForm
from stancjainfo.models import UserGroup, User


def get_all() -> List[UserGroup]:
    return UserGroup.query.all()


def delete_by_id(group_id: int):
    user_group = UserGroup.query.get_or_404(group_id)
    db.session.delete(user_group)
    db.session.commit()


def create(form: UserGroupForm):
    user_group = UserGroup(
        name=form.name.data
    )
    db.session.add(user_group)
    db.session.commit()


def get_by_id(group_id: int) -> UserGroup:
    return UserGroup.query.get_or_404(group_id)


def update_group(user_group: UserGroup, user_ids: List[int]):
    users = user_service.get_by_id_in(user_ids)
    user_group.users = users
    db.session.commit()


def shuffle_cleaning_schedule():
    bathroom_cleaner = get_bathroom_cleaner_user()
    other_cleaner = get_kitchen_other_cleaner()
    worktops_cleaner = get_kitchen_worktops_cleaner()
    restroom_cleaner = get_restroom_cleaner()
    floors_cleaner = get_floors_cleaner()
    last_cleaning_scheduled_date = definition_service.get_by_key(definition_service.__last_cleaning_schedule_set_date)

    users = shuffle_list([bathroom_cleaner, other_cleaner, worktops_cleaner, restroom_cleaner, floors_cleaner])

    set_bathroom_cleaner(users[0])
    set_kitchen_other_cleaner(users[1])
    set_worktops_cleaner(users[2])
    set_restroom_cleaner(users[3])
    set_floors_cleaner(users[4])

    definition_service.update_definition(last_cleaning_scheduled_date, datetime.now().strftime("%Y-%m-%d"))


def shuffle_list(some_list):
    randomized_list = some_list[:]
    while True:
        random.shuffle(randomized_list)
        for a, b in zip(some_list, randomized_list):
            if a == b:
                break
        else:
            return randomized_list


def get_bathroom_cleaner_user() -> User:
    definition = definition_service.get_by_key(definition_service.__cleaner_bathroom)
    if not definition:
        print('Cleaner bathroom definition not defined!')
        return None

    user = group_service.get_by_group_name(definition.value).users[0]
    return user


def get_kitchen_worktops_cleaner() -> User:
    definition = definition_service.get_by_key(definition_service.__cleaner_kitchen_worktops)
    if not definition:
        print('Cleaner kitchen worktops definition not defined!')
        return None

    user = group_service.get_by_group_name(definition.value).users[0]
    return user


def get_floors_cleaner() -> User:
    definition = definition_service.get_by_key(definition_service.__cleaner_floors)
    if not definition:
        print('Floors cleaner definition not defined!')
        return None

    user = group_service.get_by_group_name(definition.value).users[0]
    return user


def get_kitchen_other_cleaner() -> User:
    definition = definition_service.get_by_key(definition_service.__cleaner_kitchen_other)
    if not definition:
        print('Other kitchen cleaner definition not defined!')
        return None

    user = group_service.get_by_group_name(definition.value).users[0]
    return user


def get_restroom_cleaner() -> User:
    definition = definition_service.get_by_key(definition_service.__cleaner_restroom)
    if not definition:
        print('Restroom cleaner definition not defined!')
        return None

    user = group_service.get_by_group_name(definition.value).users[0]
    return user


def set_restroom_cleaner(user: User):
    definition = definition_service.get_by_key(definition_service.__cleaner_restroom)
    if not definition:
        print('Restroom cleaner definition not defined!')
        return None

    group = group_service.get_by_group_name(definition.value)
    group.users = [user]
    db.session.commit()


def set_kitchen_other_cleaner(user: User):
    definition = definition_service.get_by_key(definition_service.__cleaner_kitchen_other)
    if not definition:
        print('Kitchen other cleaner definition not defined!')
        return None

    group = group_service.get_by_group_name(definition.value)
    group.users = [user]
    db.session.commit()


def set_floors_cleaner(user: User):
    definition = definition_service.get_by_key(definition_service.__cleaner_floors)
    if not definition:
        print('Floors cleaner definition not defined!')
        return None

    group = group_service.get_by_group_name(definition.value)
    group.users = [user]
    db.session.commit()


def set_worktops_cleaner(user: User):
    definition = definition_service.get_by_key(definition_service.__cleaner_kitchen_worktops)
    if not definition:
        print('Worktops cleaner definition not defined!')
        return None

    group = group_service.get_by_group_name(definition.value)
    group.users = [user]
    db.session.commit()


def set_bathroom_cleaner(user: User):
    definition = definition_service.get_by_key(definition_service.__cleaner_bathroom)
    if not definition:
        print('Bathroom cleaner definition not defined!')
        return None

    group = group_service.get_by_group_name(definition.value)
    group.users = [user]
    db.session.commit()


def mark_place_as_cleaned(cleaning_place):
    cleaning_place_definition_key = CleaningPlaces.from_string(cleaning_place).get_definition_key()
    definition = definition_service.get_by_key(cleaning_place_definition_key)
    definition_service.update_definition(definition, 'TAK')


def is_bathroom_cleaned_str():
    definition = definition_service.get_by_key(CleaningPlaces.BATHROOM.get_definition_key())
    return definition.value


def is_kitchen_worktops_cleaned_str():
    definition = definition_service.get_by_key(CleaningPlaces.KITCHEN_WORKTOPS.get_definition_key())
    return definition.value


def is_kitchen_other_cleaned_str():
    definition = definition_service.get_by_key(CleaningPlaces.KITCHEN_OTHER.get_definition_key())
    return definition.value


def is_floor_cleaned_str():
    definition = definition_service.get_by_key(CleaningPlaces.FLOORS.get_definition_key())
    return definition.value


def is_restroom_cleaned_str():
    definition = definition_service.get_by_key(CleaningPlaces.RESTROOM.get_definition_key())
    return definition.value


def reset_rooms_cleaned_state_to_false():
    definitions_key = [e.get_definition_key() for e in CleaningPlaces]
    for key in definitions_key:
        definition = definition_service.get_by_key(key)
        definition_service.update_definition(definition, 'NIE')


def search_cleaning_room_by_user(user: User) -> str:
    cleaner = get_floors_cleaner()

    if cleaner is user:
        return 'Podłogi'

    cleaner = get_bathroom_cleaner_user()

    if cleaner is user:
        return 'Łazienka'

    cleaner = get_kitchen_other_cleaner()

    if cleaner is user:
        return 'Ściany, piekarnik, mikrofala, zlew, okap'

    cleaner = get_kitchen_worktops_cleaner()

    if cleaner is user:
        return 'Blaty, kuchenka gazowa'

    cleaner = get_restroom_cleaner()

    if cleaner is user:
        return 'Kibel i śmieci'

    return 'Nic'


class CleaningPlaces(Enum):
    KITCHEN_WORKTOPS = ('KITCHEN_WORKTOPS', 'is_kitchen_worktops_cleaned')
    KITCHEN_OTHER = ('KITCHEN_OTHER', 'is_kitchen_other_cleaned')
    BATHROOM = ('BATHROOM', 'is_bathroom_cleaned')
    RESTROOM = ('RESTROOM', 'is_restroom_cleaned')
    FLOORS = ('FLOORS', 'is_floors_cleaned')

    @classmethod
    def from_string(cls, s):
        for place in cls:
            if place.value[0] == s:
                return place
        raise ValueError(cls.__name__ + ' has no value matching "' + s + '"')

    def get_definition_key(self):
        return self.value[1]
