from enum import Enum

from flask_login import UserMixin

from stancjainfo import db, login_manager


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    name = db.Column(db.String(30))
    surname = db.Column(db.String(30))
    email = db.Column(db.String(120), unique=True, nullable=False)
    image_file = db.Column(db.String(20), default='default.jpg')
    password = db.Column(db.String(60), nullable=False)
    accepted = db.Column(db.Boolean, nullable=False, default=False)
    active = db.Column(db.Boolean, nullable=False, default=True)
    role = db.Column(db.String(20), nullable=False, default='USER')
    last_logged = db.Column(db.DateTime)
    payments = db.relationship('InternetPayment', backref='user', lazy=True)
    reset_key = db.Column(db.String(10))
    key_active = db.Column(db.Boolean, nullable=False, default=False)
    avatar_s = db.Column(db.String(64))
    avatar_m = db.Column(db.String(64))
    avatar_l = db.Column(db.String(64))

    def __repr__(self):
        return f"User('{self.username}', '{self.email}', '{self.image_file}')"

    def has_avatar(self):
        return self.avatar_s is not None or self.avatar_s is not ''


class Months(Enum):
    JAN = (1, 'Styczeń')
    FEB = (2, 'Luty')
    MAR = (3, 'Marzec')
    APR = (4, 'Kwiecień')
    MAY = (5, 'Maj')
    JUN = (6, 'Czerwiec')
    JUL = (7, 'Lipiec')
    AUG = (8, 'Sierpień')
    SEP = (9, 'Wrzesień')
    OCT = (10, 'Październik')
    NOV = (11, 'Listopad')
    DEC = (12, 'Grudzień')

    def __str__(self):
        return self.value[1]

    @classmethod
    def from_string(cls, s):
        for month in cls:
            if month.value[1] == s:
                return month
        raise ValueError(cls.__name__ + ' has no value matching "' + s + '"')

    def get_ordinal(self):
        return self.value[0]

    def __ge__(self, other):
        if self.__class__ is other.__class__:
            return self.get_ordinal() >= other.get_ordinal()
        return NotImplemented

    def __gt__(self, other):
        if self.__class__ is other.__class__:
            return self.get_ordinal() > other.get_ordinal()
        return NotImplemented

    def __le__(self, other):
        if self.__class__ is other.__class__:
            return self.get_ordinal() <= other.get_ordinal()
        return NotImplemented

    def __lt__(self, other):
        if self.__class__ is other.__class__:
            return self.get_ordinal() < other.get_ordinal()
        return NotImplemented


class InternetEntry(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    month = db.Column(db.Enum(Months), nullable=False)
    year = db.Column(db.String, nullable=False)
    payment_amount = db.Column(db.Float(precision='4,2'), nullable=False)
    date_of_payment = db.Column(db.Date, nullable=False)
    penalty = db.Column(db.Float(precision='4,2'), nullable=False)
    payments = db.relationship('InternetPayment', backref='internet_entry', lazy=True)

    @classmethod
    def update(cls, form):
        cls.month = form.month.data
        cls.year = form.year.data
        cls.payment_amount = form.payment_amount.data
        cls.date_of_payment = form.date_of_payment.data
        cls.penalty = form.penalty.data
        return cls


class InternetPayment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    remaining_amount = db.Column(db.Float(precision='4,2'), nullable=False)
    already_paid_amount = db.Column(db.Float(precision='4,2'), nullable=False)
    payment_accepted_date = db.Column(db.Date)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    internet_entry_id = db.Column(db.Integer, db.ForeignKey('internet_entry.id'), nullable=False)


class MediaEntry(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    month = db.Column(db.Enum(Months), nullable=False)
    year = db.Column(db.String, nullable=False)
    payment_amount = db.Column(db.Float(precision='4,2'), nullable=False)
    cold_water_kitchen = db.Column(db.Float(precision='4,2'), nullable=False)
    warm_water_kitchen = db.Column(db.Float(precision='4,2'), nullable=False)
    cold_water_bathroom = db.Column(db.Float(precision='4,2'), nullable=False)
    warm_water_bathroom = db.Column(db.Float(precision='4,2'), nullable=False)
    current = db.Column(db.Float(precision='4,2'), nullable=False)
    gas = db.Column(db.Float(precision='4,2'), nullable=False)
    current_refund = db.Column(db.Float(precision='4,2'), nullable=False)
    created_date = db.Column(db.Date, nullable=False)


user_group_association = db.Table(
    'user_has_group',
    db.Column('user_id', db.ForeignKey('user.id'), nullable=False),
    db.Column('user_group_id', db.ForeignKey('user_group.id'), nullable=False)
)


class UserGroup(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    users = db.relationship('User', lazy=True, secondary=user_group_association)

    def __len__(self):
        return len(self.users)


class Definition(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    key = db.Column(db.String(50), nullable=False)
    value = db.Column(db.String(255), nullable=False)


class DebtChangeType(Enum):
    LEND = 'LEND'  # value lend to someone
    RETURN = 'RETURN'  # value returned


class LocatorDebt(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_from_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    user_to_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    amount = db.Column(db.Float(precision='4,2'), nullable=False)
    remaining_amount = db.Column(db.Float(precision='4,2'), nullable=False)
    debt_change_type = db.Column(db.Enum(DebtChangeType), nullable=False)
    created_date = db.Column(db.Date, nullable=False)
    active = db.Column(db.Boolean, nullable=False, default=True)
    comment = db.Column(db.String(255))

    'adds amounts of two LocatorDebt classes'

    def __add__(self, other):
        return self.amount + other.amount

    'adds a value to this class amount'

    def __radd__(self, other):
        return self.amount + other


class SmogMeasurement(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    pm25 = db.Column(db.Float(precision='4,2'), nullable=False)
    pm10 = db.Column(db.Float(precision='4,2'), nullable=False)
    timestamp = db.Column(db.DateTime)


class BmeMeasurement(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    temperature = db.Column(db.Float(precision='4,2'), nullable=False)
    humidity = db.Column(db.Float(precision='5,2'), nullable=False)
    pressure = db.Column(db.Float(precision='6,2'), nullable=False)
    timestamp = db.Column(db.DateTime)


class ChatMessage(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    message = db.Column(db.String(255), nullable=False)
    username = db.Column(db.String(20), nullable=False)
    avatar_url = db.Column(db.String(64))
    namespace = db.Column(db.String(30))
    room = db.Column(db.String(30))
    timestamp = db.Column(db.DateTime)
