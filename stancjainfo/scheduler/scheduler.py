import threading
import time
from typing import List

import schedule

from stancjainfo.scheduler.jobs.abstract_job import AbstractJob


class Scheduler(object):

    def __init__(self):
        self.jobs: List[AbstractJob] = []

    def init_jobs(self):
        for job in self.jobs:
            if job.should_do_job():
                job.get_time_interval_definition().do(job.work)

    @staticmethod
    def run_continuously(interval=1):
        """Continuously run, while executing pending jobs at each elapsed
        time interval.
        @return cease_continuous_run: threading.Event which can be set to
        cease continuous run.
        Please note that it is *intended behavior that run_continuously()
        does not run missed jobs*. For example, if you've registered a job
        that should run every minute and you set a continuous run interval
        of one hour then your job won't be run 60 times at each interval but
        only once.
        """
        cease_continuous_run = threading.Event()

        class ScheduleThread(threading.Thread):
            @classmethod
            def run(cls):
                while not cease_continuous_run.is_set():
                    schedule.run_pending()
                    time.sleep(interval)

        continuous_thread = ScheduleThread()
        continuous_thread.start()
        return cease_continuous_run

    def add_job(self, job: AbstractJob):
        self.jobs.append(job)