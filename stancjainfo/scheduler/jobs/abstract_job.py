from abc import ABC, abstractmethod

import schedule


class AbstractJob(ABC):

    def __init__(self):
        super().__init__()

    @abstractmethod
    def work(self):
        pass

    @abstractmethod
    def get_time_interval_definition(self) -> schedule.Job:
        pass

    @abstractmethod
    def should_do_job(self) -> bool:
        pass
