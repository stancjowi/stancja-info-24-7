import time
from datetime import datetime

import schedule
from adafruit_blinka.agnostic import board_id

from stancjainfo import bme280_service
from stancjainfo.scheduler.jobs.abstract_job import AbstractJob

RASPBERRY_PI = 'RASPBERRY_PI_3B_PLUS'


class MeasureTemperatureHumidityPressure(AbstractJob):

    @staticmethod
    def print_values(timing, values):
        unit = 'µg/m³'
        # print("Waited %d secs\nValues measured in %s:    PM2.5  " %
        #       (timing, unit), values[1], ", PM10 ", values[0])

    def should_do_job(self) -> bool:
        return True

    def get_time_interval_definition(self) -> schedule.Job:
        return schedule.every(3).minutes

    def work(self):
        print("Measuring Temperature Humidity and Pressure")
        print("Timestamp: ", datetime.now())
        time.sleep(1)

        if board_id is not RASPBERRY_PI:
            print("Cant perform the measurement. Device is not an raspberry pi!")
            return
        else:
            import board
            import busio
            import adafruit_bme280

        i2c = busio.I2C(board.SCL, board.SDA)
        bme280 = adafruit_bme280.Adafruit_BME280_I2C(i2c)

        print("Beginning the measurement...")

        temperature = bme280.temperature
        humidity = bme280.humidity
        pressure = bme280.pressure

        print("Measurement completed")
        try:
            bme280_service.create_measurement(timestamp=datetime.now(), temperature=temperature, humidity=humidity,
                                              pressure=pressure)
        except Exception as e:
            print("Exception occurred: ", e)

        time.sleep(1)
