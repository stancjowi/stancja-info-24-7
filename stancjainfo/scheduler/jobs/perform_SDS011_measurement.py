import time
from datetime import datetime

import schedule

from stancjainfo import smog_service
from stancjainfo.scheduler.jobs.abstract_job import AbstractJob
from stancjainfo.smog_sensor.sds011 import SDS011
from stancjainfo.smog_sensor.smogdata import SmogData


class MeasureSmog(AbstractJob):

    @staticmethod
    def print_values(timing, values):
        unit = 'µg/m³'
        # print("Waited %d secs\nValues measured in %s:    PM2.5  " %
        #       (timing, unit), values[1], ", PM10 ", values[0])

    def should_do_job(self) -> bool:
        return True

    def get_time_interval_definition(self) -> schedule.Job:
        return schedule.every(30).minutes

    def work(self):
        print("Measuring smog")
        print("Timestamp: ", datetime.now())
        time.sleep(3)  # serial port time bug
        sensor = SDS011("/dev/ttyUSB0", timeout=2, unit_of_measure=SDS011.UnitsOfMeasure.MassConcentrationEuropean)

        sensor.workstate = SDS011.WorkStates.Measuring

        print("warmup 60s...")
        time.sleep(60)

        print("Beginning the measurement...")
        x = 60
        results = []

        for i in range(1, x):
            values = sensor.get_values()
            if values is not None:
                self.print_values(1, values)
                results.append((values[1], values[0]))
            else:
                print("Could not read values")
            time.sleep(1)

        print("Measurement completed")
        count = len(results)
        if count:
            print("Saving results")
            pm25_values = sum(x for x, y in results)
            pm10_values = sum(y for x, y in results)
            pm25 = pm25_values / count
            pm10 = pm10_values / count
            data = SmogData(datetime.now(), pm25, pm10)

            try:
                smog_service.create_measurement(data)
            except Exception as e:
                print("Exception occurred: ", e)
        else:
            print("No values to save")

        print("Now going to sleeping state")
        sensor.workstate = SDS011.WorkStates.Sleeping
        time.sleep(5)
