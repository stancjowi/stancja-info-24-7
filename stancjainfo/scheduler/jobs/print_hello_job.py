import subprocess

import schedule

from stancjainfo.scheduler.jobs.abstract_job import AbstractJob


class PrintHelloJob(AbstractJob):
    def should_do_job(self) -> bool:
        return True

    def get_time_interval_definition(self) -> schedule.Job:
        return schedule.every(1).minute

    def work(self):
        bashCommand = "whoami"
        process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
        output, error = process.communicate()
        if error:
            print("error!", error)
        print("HI! My name is: ", output)
