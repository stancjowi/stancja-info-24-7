from flask import render_template, Blueprint, send_from_directory, current_app, redirect, url_for, session
from flask_login import login_required, current_user

from stancjainfo import user_service, avatars, cleaning_service, group_service
from stancjainfo.forms import UploadAvatarForm, CropAvatarForm

profile_module = Blueprint('profile_module', __name__)


@profile_module.route('/my-profile')
@login_required
def my_profile():
    user = user_service.get_by_id(current_user.id)
    cleaning_room = cleaning_service.search_cleaning_room_by_user(user)
    is_internet_debtor = group_service.is_user_in_internet_debtors_list(user)
    return render_template('user_profile.html', user=user, cleaning_room=cleaning_room, is_internet_debtor=is_internet_debtor)


@profile_module.route('/avatars/<path:filename>')
def get_avatar(filename):
    return send_from_directory(current_app.config['AVATARS_SAVE_PATH'], filename)


@profile_module.route('/generate_avatars', methods=['POST'])
@login_required
def generate_avatars():
    user = user_service.get_by_id(current_user.id)
    user_service.generate_avatars(user)
    return redirect(url_for('profile_module.my_profile'))


@profile_module.route('/upload_avatar', methods=['GET', 'POST'])
@login_required
def upload_avatar():
    form = UploadAvatarForm()
    if form.validate_on_submit():
        file = form.image.data
        print(file)
        raw_filename = avatars.save_avatar(file)
        session['raw_filename'] = raw_filename  # you will need to store this filename in database in reality
        return redirect(url_for('profile_module.crop'))
    return render_template('upload_avatar.html', form=form)


@profile_module.route('/crop', methods=['GET', 'POST'])
@login_required
def crop():
    form = CropAvatarForm()
    if form.validate_on_submit():
        x = form.x.data
        y = form.y.data
        w = form.w.data
        h = form.h.data
        filenames = avatars.crop_avatar(session['raw_filename'], x, y, w, h)
        user = user_service.get_current_user()
        user_service.add_avatars_to_user(user, filenames)
        return redirect(url_for('profile_module.my_profile'))
    return render_template('crop_avatar.html', form=form)