from typing import List

from stancjainfo import db
from stancjainfo.models import ChatMessage


def get_by_id(id: int) -> ChatMessage:
    return ChatMessage.query.get_or_404(id)


def get_by_namespace_limit(namespace: str, limit: int) -> List[ChatMessage]:
    return ChatMessage.query.filter_by(namespace=namespace).order_by(ChatMessage.timestamp.desc()).limit(limit).all()


def create_message(username: str, message: str, namespace: str, room: str, date, avatar_url: str = ''):

    chat_message = ChatMessage(
        username=username,
        message=message,
        namespace=namespace,
        room=room,
        timestamp=date,
        avatar_url=avatar_url
    )
    db.session.add(chat_message)
    db.session.commit()
