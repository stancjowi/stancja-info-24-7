from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired, FileAllowed
from wtforms import StringField, PasswordField, SubmitField, BooleanField, TextAreaField, SelectField, DecimalField, \
    DateField, SelectMultipleField, HiddenField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError, InputRequired, NumberRange

from stancjainfo.models import User, InternetEntry, MediaEntry


class RegistrationForm(FlaskForm):
    username = StringField('Użytkownik',
                           validators=[DataRequired(), Length(min=2, max=20)])
    name = StringField('Imię',
                           validators=[DataRequired(), Length(min=2, max=30)])
    surname = StringField('Nazwisko',
                           validators=[DataRequired(), Length(min=2, max=30)])
    email = StringField('Email',
                        validators=[DataRequired(), Email()])
    password = PasswordField('Hasło',
                             validators=[DataRequired(), Length(min=2, max=20)])
    confirm_password = PasswordField('Potwierdź hasło',
                                     validators=[DataRequired(), EqualTo('password'), Length(min=2, max=20)])
    submit = SubmitField('Zarejestruj')

    def validate_username(self, username):
        user = User.query.filter_by(username=str(username.data).lower()).first()
        if user:
            raise ValidationError('That username is already taken')

    def validate_email(self, email):
        user = User.query.filter_by(email=str(email.data).lower()).first()
        if user:
            raise ValidationError('That email is already taken')


class ResetPasswordForm(FlaskForm):
    email = StringField('Email',
                        validators=[DataRequired(), Email()])
    submit = SubmitField('Wyślij')


    def validate_email(self, email):
        user = User.query.filter_by(email=str(email.data).lower()).first()
        if not user:
            raise ValidationError('Email nie istnieje w systemie')


class NewPasswordForm(FlaskForm):
    password = PasswordField('Hasło',
                             validators=[DataRequired(), Length(min=2, max=20)])
    confirm_password = PasswordField('Potwierdź hasło',
                                     validators=[DataRequired(), EqualTo('password'), Length(min=2, max=20)])
    submit = SubmitField('Wyślij')


class UpdateUserForm(FlaskForm):
    username = StringField('Użytkownik',
                           validators=[DataRequired(), Length(min=2, max=20)])
    name = StringField('Imię',
                           validators=[DataRequired(), Length(min=2, max=30)])
    surname = StringField('Nazwisko',
                           validators=[DataRequired(), Length(min=2, max=30)])
    email = StringField('Email',
                        validators=[DataRequired(), Email()])
    submit = SubmitField('Zaktualizuj')


class LoginForm(FlaskForm):
    username = StringField('Użytkownik',
                           validators=[DataRequired(), Length(min=2, max=20)])
    password = PasswordField('Hasło',
                             validators=[DataRequired(), Length(min=2, max=20)])
    remember = BooleanField('Pamiętaj mnie')
    submit = SubmitField('Zaloguj')


class PostForm(FlaskForm):
    title = StringField('Nagłówek', validators=[DataRequired()])
    content = TextAreaField('Tekst', validators=[DataRequired()])
    submit = SubmitField('Dodaj')


class InternetEntryForm(FlaskForm):
    month = SelectField('Miesiąc', validators=[DataRequired()], coerce=str, choices=[
        ('JAN', 'Styczeń'),
        ('FEB', 'Luty'),
        ('MAR', 'Marzec'),
        ('APR', 'Kwiecień'),
        ('MAY', 'Maj'),
        ('JUN', 'Czerwiec'),
        ('JUL', 'Lipiec'),
        ('AUG', 'Sierpień'),
        ('SEP', 'Wrzesień'),
        ('OCT', 'Październik'),
        ('NOV', 'Listopad'),
        ('DEC', 'Grudzień')])
    year = SelectField('Rok', validators=[DataRequired()], coerce=int, choices=[
        (2017, '2017'),
        (2018, '2018'),
        (2019, '2019'),
        (2020, '2020')])
    payment_amount = DecimalField('Należność', validators=[InputRequired()], places=2)
    date_of_payment = DateField('Data zapłaty', validators=[DataRequired()])
    penalty = DecimalField('Kara', validators=[InputRequired()], places=2)
    submit = SubmitField('Dodaj')

    def validate_month(self, month):
        entry = InternetEntry.query.filter_by(month=month.data, year=int(self.year.data)).first()
        if entry:
            raise ValidationError('Entry for that month already exists')


class UpdateInternetEntryForm(FlaskForm):
    month = SelectField('Miesiąc', validators=[DataRequired()], coerce=str, choices=[
        ('JAN', 'Styczeń'),
        ('FEB', 'Luty'),
        ('MAR', 'Marzec'),
        ('APR', 'Kwiecień'),
        ('MAY', 'Maj'),
        ('JUN', 'Czerwiec'),
        ('JUL', 'Lipiec'),
        ('AUG', 'Sierpień'),
        ('SEP', 'Wrzesień'),
        ('OCT', 'Październik'),
        ('NOV', 'Listopad'),
        ('DEC', 'Grudzień')])
    year = SelectField('Rok', validators=[DataRequired()], coerce=int, choices=[
        (2017, '2017'),
        (2018, '2018'),
        (2019, '2019'),
        (2020, '2020')])
    payment_amount = DecimalField('Należność', validators=[InputRequired()], places=2)
    date_of_payment = DateField('Data zapłaty', validators=[DataRequired()])
    penalty = DecimalField('Kara', validators=[InputRequired()], places=2)
    submit = SubmitField('Zaktualizuj')


class MediaEntryForm(FlaskForm):
    month = SelectField('Miesiąc', validators=[DataRequired()], coerce=str, choices=[
        ('JAN', 'Styczeń'),
        ('FEB', 'Luty'),
        ('MAR', 'Marzec'),
        ('APR', 'Kwiecień'),
        ('MAY', 'Maj'),
        ('JUN', 'Czerwiec'),
        ('JUL', 'Lipiec'),
        ('AUG', 'Sierpień'),
        ('SEP', 'Wrzesień'),
        ('OCT', 'Październik'),
        ('NOV', 'Listopad'),
        ('DEC', 'Grudzień')])
    year = SelectField('Rok', validators=[DataRequired()], coerce=int, choices=[
        (2017, '2017'),
        (2018, '2018'),
        (2019, '2019'),
        (2020, '2020')])
    payment_amount = DecimalField('Należność', validators=[InputRequired()], places=2)
    cold_water_kitchen = DecimalField('Zimna woda kuchnia', validators=[InputRequired()])
    warm_water_kitchen = DecimalField('Ciepła woda kuchnia', validators=[InputRequired()])
    cold_water_bathroom = DecimalField('Zimna woda łazienka', validators=[InputRequired()])
    warm_water_bathroom = DecimalField('Ciepła woda łazienka', validators=[InputRequired()])
    current = DecimalField('Prąd', validators=[InputRequired()])
    gas = DecimalField('Gaz', validators=[InputRequired()])
    current_refund = DecimalField('Zwrot za prąd', validators=[InputRequired()])
    created_date = DateField('Data spisania', validators=[DataRequired()])
    submit = SubmitField('Dodaj')

    def validate_month(self, month):
        entry = MediaEntry.query.filter_by(month=month.data, year=int(self.year.data)).first()
        if entry:
            raise ValidationError('Entry for that month already exists')


class UpdateMediaEntryForm(FlaskForm):
    month = SelectField('Miesiąc', validators=[DataRequired()], coerce=str, choices=[
        ('JAN', 'Styczeń'),
        ('FEB', 'Luty'),
        ('MAR', 'Marzec'),
        ('APR', 'Kwiecień'),
        ('MAY', 'Maj'),
        ('JUN', 'Czerwiec'),
        ('JUL', 'Lipiec'),
        ('AUG', 'Sierpień'),
        ('SEP', 'Wrzesień'),
        ('OCT', 'Październik'),
        ('NOV', 'Listopad'),
        ('DEC', 'Grudzień')])
    year = SelectField('Rok', validators=[DataRequired()], coerce=int, choices=[
        (2017, '2017'),
        (2018, '2018'),
        (2019, '2019'),
        (2020, '2020')])
    payment_amount = DecimalField('Należność', validators=[InputRequired()], places=2)
    cold_water_kitchen = DecimalField('Zimna woda kuchnia', validators=[InputRequired()])
    warm_water_kitchen = DecimalField('Ciepła woda kuchnia', validators=[InputRequired()])
    cold_water_bathroom = DecimalField('Zimna woda łazienka', validators=[InputRequired()])
    warm_water_bathroom = DecimalField('Ciepła woda łazienka', validators=[InputRequired()])
    current = DecimalField('Prąd', validators=[InputRequired()])
    gas = DecimalField('Gaz', validators=[InputRequired()])
    current_refund = DecimalField('Zwrot za prąd', validators=[InputRequired()])
    created_date = DateField('Data spisania', validators=[DataRequired()])
    submit = SubmitField('Dodaj')


class UserGroupForm(FlaskForm):
    name = StringField('Nazwa', validators=[DataRequired(), Length(min=2, max=50)])
    submit = SubmitField('Dodaj')


class AddUsersToGroupForm(FlaskForm):
    users = SelectMultipleField('Użytkownicy', coerce=int, choices=[])
    submit = SubmitField('OK')


class DefinitionForm(FlaskForm):
    key = StringField('Klucz', validators=[DataRequired(), Length(min=1, max=50)])
    value = StringField('Wartość', validators=[DataRequired(), Length(min=1, max=255)])
    submit = SubmitField('OK')


class AddDebtForm(FlaskForm):
    who = SelectField(' ', coerce=int, choices=[], validators=[InputRequired()])
    amount = DecimalField('Jest mi winien', validators=[InputRequired(), NumberRange(min=0.01, message='Liczba musi być większa od zera')])
    comment = StringField('Komentarz', validators=[Length(max=225)])
    submit = SubmitField('OK')


class RemoveDebtForm(FlaskForm):
    who = SelectField(' ', coerce=int, choices=[], validators=[InputRequired()])
    amount = DecimalField('Oddał', validators=[InputRequired(), NumberRange(min=0.01, message='Liczba musi być większa od zera')])
    comment = StringField('Komentarz', validators=[Length(max=225)])
    submit = SubmitField('OK')


class UploadAvatarForm(FlaskForm):
    image = FileField('Upload (<=5M)', validators=[
        FileRequired(),
        FileAllowed(['jpg', 'png', 'jpeg'], 'The file format should be .jpg, .jpeg or .png.')
    ])
    submit = SubmitField('Załaduj')


class CropAvatarForm(FlaskForm):
    x = HiddenField()
    y = HiddenField()
    w = HiddenField()
    h = HiddenField()
    submit = SubmitField('Przytnij')
