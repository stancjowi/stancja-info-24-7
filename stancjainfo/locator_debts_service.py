from datetime import datetime
from functools import reduce
from typing import List

from flask_login import current_user

from stancjainfo import db, user_service
from stancjainfo.forms import AddDebtForm, RemoveDebtForm
from stancjainfo.models import LocatorDebt, DebtChangeType


class DebtTableRecord:  # todo wyniesc

    def __init__(self, user_from_id, user_to_id, value, user_name):
        self.user_name = user_name
        self.user_from_id = user_from_id
        self.user_to_id = user_to_id
        self.value = value


def get_debts_table(users: List[tuple]):
    result_array = []
    for user in users:
        one_user_array = []
        for other in users:
            if other is user:
                debt_record = DebtTableRecord(user[0], other[0], 0.0, user[1])
                one_user_array.append(debt_record)
                continue
            debts_for_other = LocatorDebt.query.filter_by(user_from_id=user[0], user_to_id=other[0]).all()
            reverser = 1
            if not debts_for_other:
                reverser = -1
                debts_for_other = LocatorDebt.query.filter_by(user_to_id=user[0], user_from_id=other[0]).all()

            lend = list(filter(lambda debt: debt.debt_change_type is DebtChangeType.LEND, debts_for_other))
            returned = list(filter(lambda debt: debt.debt_change_type is DebtChangeType.RETURN, debts_for_other))
            lend_sum = reduce((lambda x, y: x + y), lend, 0.0)
            returned_sum = reduce((lambda x, y: x + y), returned, 0.0)

            value = (lend_sum - returned_sum) * reverser
            debt_record = DebtTableRecord(user[0], other[0], value, user[1])
            one_user_array.append(debt_record)
        result_array.append(one_user_array)

    return result_array


def get_latest() -> List[LocatorDebt]:
    return LocatorDebt.query.order_by(LocatorDebt.created_date.desc()).limit(20).all()


def create_lend_debt(form: AddDebtForm):
    user_to_id = user_service.get_by_id(form.who.data).id
    debt = LocatorDebt(
        user_from_id=current_user.id,
        user_to_id=user_to_id,
        amount=form.amount.data,
        remaining_amount=form.amount.data,
        debt_change_type=DebtChangeType.LEND,
        created_date=datetime.now(),
        active=True,
        comment=form.comment.data)

    db.session.add(debt)
    db.session.commit()


def create_return_debt(form: RemoveDebtForm):
    user_to_id = user_service.get_by_id(form.who.data).id
    debt = LocatorDebt(
        user_from_id=current_user.id,
        user_to_id=user_to_id,
        amount=form.amount.data,
        remaining_amount=form.amount.data,
        debt_change_type=DebtChangeType.RETURN,
        created_date=datetime.now(),
        active=True,
        comment=form.comment.data)

    db.session.add(debt)
    db.session.commit()