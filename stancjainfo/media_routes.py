from flask import render_template, Blueprint
from flask_login import login_required

from stancjainfo import media_service
from stancjainfo import user_service

media_module = Blueprint('media_module', __name__)


@media_module.route('/media')
@login_required
def media():
    media_entry = media_service.get_latest()
    payers_count = user_service.get_media_payers_count()
    per_capita = 0
    if payers_count is not 0:
        per_capita = (media_entry.payment_amount - media_entry.current_refund) / payers_count
    return render_template('media_entry.html', media_entry=media_entry, per_capita=per_capita)


@media_module.route('/media/history')
@login_required
def media_history():
    media_entries = media_service.get_all()
    return render_template('media_history.html', media_entries=media_entries)


@media_module.route('/media/<int:media_id>')
@login_required
def media_details(media_id):
    media_entry = media_service.get_by_id(media_id)
    payers_count = user_service.get_media_payers_count()
    per_capita = 0
    if payers_count is not 0:
        per_capita = (media_entry.payment_amount - media_entry.current_refund) / payers_count
    return render_template('media_details.html', media_entry=media_entry, per_capita=per_capita)
