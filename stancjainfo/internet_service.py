from typing import List

from flask import flash
from wtforms import ValidationError

from stancjainfo import db
from stancjainfo.forms import InternetEntryForm, UpdateInternetEntryForm
from stancjainfo.models import InternetEntry


def get_by_id(entry_id: int) -> InternetEntry:
    return InternetEntry.query.get_or_404(entry_id)


def get_latest() -> InternetEntry:
    return InternetEntry.query.order_by(InternetEntry.date_of_payment.desc()).first()


def get_all_sorted_by_time() -> List[InternetEntry]:
    return InternetEntry.query.order_by(InternetEntry.date_of_payment.desc()).all()


def get_all() -> List[InternetEntry]:
    return InternetEntry.query.all()


def create_entry(form: InternetEntryForm):
    internet_entry = InternetEntry(
        month=form.month.data,
        year=form.year.data,
        payment_amount=form.payment_amount.data,
        date_of_payment=form.date_of_payment.data,
        penalty=form.penalty.data)
    db.session.add(internet_entry)
    db.session.commit()


def update_entry(internet_entry: InternetEntry, form: UpdateInternetEntryForm):
    existing_entry = InternetEntry.query.filter_by(month=form.month.data, year=int(form.year.data)).first()
    if existing_entry and existing_entry is not internet_entry:
        flash("Nie można edytować tego miesiąca - opłata za ten miesiąc istnieje", 'error')
        raise ValidationError('Entry for that month already exists')
    internet_entry.month = form.month.data
    internet_entry.year = form.year.data
    internet_entry.payment_amount = form.payment_amount.data
    internet_entry.date_of_payment = form.date_of_payment.data
    internet_entry.penalty = form.penalty.data
    db.session.commit()


def fill_update_form(internet_entry: InternetEntry, form: UpdateInternetEntryForm) -> UpdateInternetEntryForm:
    form.month.data = internet_entry.month.name
    form.year.data = int(internet_entry.year)
    form.payment_amount.data = internet_entry.payment_amount
    form.date_of_payment.data = internet_entry.date_of_payment
    form.date_of_payment.data = internet_entry.date_of_payment
    form.penalty.data = internet_entry.penalty
    return form


def delete_by_id(entry_id: int):
    internet_entry = InternetEntry.query.get_or_404(entry_id)
    db.session.delete(internet_entry)
    db.session.commit()