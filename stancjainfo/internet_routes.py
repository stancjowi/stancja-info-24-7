from flask import render_template, Blueprint
from flask_login import login_required

import stancjainfo.internet_service as internet_service

internet_module = Blueprint('internet_module', __name__)


@internet_module.route('/internet')
@login_required
def internet():
    internet_entry = internet_service.get_latest()
    payments = internet_entry.payments
    per_capita = internet_entry.payment_amount / 5
    return render_template('internet_entry.html', internet_entry=internet_entry, per_capita=per_capita,
                           payments=payments)


@internet_module.route('/internet/history')
@login_required
def internet_history():
    internet_entries = internet_service.get_all_sorted_by_time()
    return render_template('internet_history.html', internet_entries=internet_entries)


@internet_module.route('/internet/<int:internet_id>')
@login_required
def internet_details(internet_id):
    internet_entry = internet_service.get_by_id(internet_id)
    per_capita = internet_entry.payment_amount / 5  # todo
    return render_template('internet_details.html', internet_entry=internet_entry, per_capita=per_capita)
