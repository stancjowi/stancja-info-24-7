from typing import List

from stancjainfo import db
from stancjainfo.models import BmeMeasurement


def get_by_id(id: int) -> BmeMeasurement:
    return BmeMeasurement.query.get_or_404(id)


def get_latest() -> BmeMeasurement:
    return BmeMeasurement.query.order_by(BmeMeasurement.timestamp.desc()).first()


def get_first_20() -> List[BmeMeasurement]:
    return BmeMeasurement.query.order_by(BmeMeasurement.timestamp.desc()).limit(20)


def create_measurement(timestamp, temperature=0, humidity=0, pressure=0):
    bme280_measurement = BmeMeasurement(
        timestamp=timestamp,
        temperature=temperature,
        humidity=humidity,
        pressure=pressure
    )
    db.session.add(bme280_measurement)
    db.session.commit()
