from typing import List

from stancjainfo import db
from stancjainfo.models import SmogMeasurement
from stancjainfo.smog_sensor.smogdata import SmogData


def get_by_id(id: int) -> SmogMeasurement:
    return SmogMeasurement.query.get_or_404(id)


def get_latest() -> SmogMeasurement:
    return SmogMeasurement.query.order_by(SmogMeasurement.timestamp.desc()).first()


def get_first_20() -> List[SmogMeasurement]:
    return SmogMeasurement.query.order_by(SmogMeasurement.timestamp.desc()).limit(20)


def create_measurement(data: SmogData):
    smog_measurement = SmogMeasurement(
        timestamp=data.timestamp,
        pm25=data.pm25,
        pm10=data.pm10)
    db.session.add(smog_measurement)
    db.session.commit()
