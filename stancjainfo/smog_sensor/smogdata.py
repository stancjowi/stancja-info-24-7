class SmogData(object):

    def __init__(self, timestamp, pm25, pm10):
        self.timestamp = timestamp
        self.pm25 = pm25
        self.pm10 = pm10

