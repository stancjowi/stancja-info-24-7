from typing import List

from stancjainfo import db, definition_service
from stancjainfo import user_service
from stancjainfo.forms import UserGroupForm
from stancjainfo.models import UserGroup, User


def get_all() -> List[UserGroup]:
    return UserGroup.query.all()


def delete_by_id(group_id: int):
    user_group = UserGroup.query.get_or_404(group_id)
    db.session.delete(user_group)
    db.session.commit()


def create(form: UserGroupForm):
    user_group = UserGroup(
        name=form.name.data
    )
    db.session.add(user_group)
    db.session.commit()


def get_by_id(group_id: int) -> UserGroup:
    return UserGroup.query.get_or_404(group_id)


def update_group(user_group: UserGroup, user_ids: List[int]):
    users = user_service.get_by_id_in(user_ids)
    user_group.users = users
    db.session.commit()


def get_by_group_name(group_name: str) -> UserGroup:
    return UserGroup.query.filter_by(name=group_name).first()


def is_user_in_internet_debtors_list(user: User) -> bool:
    group_name = definition_service.get_by_key(definition_service.__debtors_key).value
    group = get_by_group_name(group_name).users
    return user in group
