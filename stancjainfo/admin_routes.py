from flask import render_template, flash, redirect, url_for, request, Blueprint
from flask_login import current_user, login_required

import stancjainfo.definition_service as definition_service
import stancjainfo.group_service as group_service
import stancjainfo.internet_service as internet_service
import stancjainfo.mail_service as mail_service
import stancjainfo.media_service as media_service
import stancjainfo.user_service as user_service
from stancjainfo import cleaning_service, constants
from stancjainfo.forms import MediaEntryForm, \
    UpdateMediaEntryForm, UpdateInternetEntryForm, UpdateUserForm, InternetEntryForm, UserGroupForm, \
    AddUsersToGroupForm, DefinitionForm
from stancjainfo.models import User, InternetEntry, MediaEntry

__prefix = '/adminpanel'
admin = Blueprint('admin', __name__, url_prefix=__prefix)


def is_current_user_an_admin():
    return current_user.is_authenticated and User.query.get(current_user.id).role == 'ADMIN'


@admin.route('/')
@login_required
def admin_panel():
    if not is_current_user_an_admin():
        return redirect(url_for('hello_world'))
    return render_template('admin_panel.html')


@admin.route('/users')
@login_required
def admin_users():
    if not is_current_user_an_admin():
        return redirect(url_for('hello_world'))
    users = user_service.get_all()
    return render_template('admin_user.html', users=users)


@admin.route('/users/accept/<int:user_id>', methods=['POST'])
@login_required
def accept_user(user_id):
    if not is_current_user_an_admin():
        return redirect(url_for('hello_world'))
    user_service.accept_user(user_id)
    return redirect(url_for('admin.admin_users'))


@admin.route('/users/activate/<int:user_id>', methods=['POST'])
@login_required
def activate_user(user_id):
    if not is_current_user_an_admin():
        return redirect(url_for('hello_world'))
    user_service.activate_user(user_id)
    return redirect(url_for('admin.admin_users'))


@admin.route('/users/view/<int:user_id>', methods=['GET'])
@login_required
def show_user_details(user_id):
    if not is_current_user_an_admin():
        return redirect(url_for('hello_world'))
    user = user_service.get_by_id(user_id)
    return render_template('admin_view_user.html', user=user)


@admin.route('/users/<int:user_id>/update', methods=['GET', 'POST'])
@login_required
def update_user(user_id):
    if not is_current_user_an_admin():
        return redirect(url_for('hello_world'))
    user = User.query.get_or_404(user_id)
    form = UpdateUserForm()
    if form.validate_on_submit():
        user_service.update_user(user, form)
        flash("Użytkownik został zmodyfikowany", 'success')
        return redirect(url_for('admin.admin_users'))
    elif request.method == 'GET':
        form = user_service.fill_update_form(user, form)

    return render_template('admin_edit_user.html', form=form)


@admin.route('/internet')
@login_required
def admin_internet():
    if not is_current_user_an_admin():
        return redirect(url_for('hello_world'))
    internet_entries = internet_service.get_all()
    return render_template('admin_internet.html', internet_entries=internet_entries)


@admin.route('/internet/new', methods=['GET', 'POST'])
@login_required
def admin_add_internet_entry():
    if not is_current_user_an_admin():
        return redirect(url_for('hello_world'))
    form = InternetEntryForm()
    if form.validate_on_submit():
        internet_service.create_entry(form)
        mail_service.send_to_all_active_users(constants.ADD_INTERNET_ENTRY_SUBJECT, constants.ADD_INTERNET_ENTRY_BODY)
        flash('Wpis został utworzony', 'success')
        return redirect(url_for('admin.admin_internet'))
    return render_template('create_internet_entry.html', form=form)


@admin.route('/internet/<int:entry_id>', methods=['GET'])
@login_required
def show_admin_internet_entry(entry_id):
    if not is_current_user_an_admin():
        return redirect(url_for('hello_world'))
    internet_entry = internet_service.get_by_id(entry_id)
    payments = internet_entry.payments
    users_that_have_paid = []
    for payment in payments:
        users_that_have_paid.append(User.query.get(payment.user_id))
    all_active_users = User.query.filter_by(accepted=True)
    users_that_havent_paid = [x for x in all_active_users if x not in users_that_have_paid]
    per_capita = internet_entry.payment_amount / 5  # tu bedzie mogl dodawac rzeczy
    return render_template('admin_internet_entry.html', internet_entry=internet_entry, per_capita=per_capita,
                           payments=payments, users_that_havent_paid=users_that_havent_paid,
                           users_that_paid=users_that_have_paid)


@admin.route('/internet/<int:entry_id>/update', methods=['GET', 'POST'])
@login_required
def update_internet_entry(entry_id):
    if not is_current_user_an_admin():
        return redirect(url_for('hello_world'))
    internet_entry = InternetEntry.query.get_or_404(entry_id)
    form = UpdateInternetEntryForm()
    if form.validate_on_submit():
        internet_service.update_entry(internet_entry, form)
        flash("Opłata za media zmodyfikowana", 'success')
        return redirect(url_for('admin.admin_internet'))
    elif request.method == 'GET':
        form = internet_service.fill_update_form(internet_entry, form)
    return render_template('create_internet_entry.html', internet_entry=internet_entry, form=form)


@admin.route('/internet/<int:entry_id>/delete', methods=['POST'])
@login_required
def delete_internet_entry(entry_id):
    if not is_current_user_an_admin():
        return redirect(url_for('hello_world'))
    internet_service.delete_by_id(entry_id)
    flash("Opłata za internet została usunięta", 'success')
    return redirect(url_for('admin.admin_internet'))


@admin.route('/media/<int:entry_id>', methods=['GET'])
@login_required
def show_admin_media_entry(entry_id):
    if not is_current_user_an_admin():
        return redirect(url_for('hello_world'))
    media_entry = media_service.get_by_id(entry_id)
    per_capita = (media_entry.payment_amount - media_entry.current_refund) / 5
    return render_template('media_details.html', media_entry=media_entry, per_capita=per_capita)


@admin.route('/media')
@login_required
def admin_media():
    if not is_current_user_an_admin():
        return redirect(url_for('hello_world'))
    media_entries = media_service.get_all()
    return render_template('admin_media.html', media_entries=media_entries)


@admin.route('/media/new', methods=['GET', 'POST'])
@login_required
def admin_add_media_entry():
    if not is_current_user_an_admin():
        return redirect(url_for('hello_world'))
    form = MediaEntryForm()
    if form.validate_on_submit():
        media_service.create_entry(form)
        mail_service.send_to_all_active_users(constants.ADD_MEDIA_ENTRY_SUBJECT, constants.ADD_MEDIA_ENTRY_BODY)
        flash('Wpis został utworzony', 'success')
        return redirect(url_for('admin.admin_media'))
    return render_template('create_media_entry.html', form=form)


@admin.route('/media/<int:entry_id>/update', methods=['GET', 'POST'])
@login_required
def update_media_entry(entry_id):
    if not is_current_user_an_admin():
        return redirect(url_for('hello_world'))
    media_entry = MediaEntry.query.get_or_404(entry_id)
    form = UpdateMediaEntryForm()
    if form.validate_on_submit():
        media_service.update_entry(media_entry, form)
        flash("Opłata za media zmodyfikowana", 'success')
        return redirect(url_for('admin.admin_media'))
    elif request.method == 'GET':
        form = media_service.fill_update_form(media_entry, form)
    return render_template('create_media_entry.html', media_entry=media_entry, form=form)


@admin.route('/media/<int:entry_id>/delete', methods=['POST'])
@login_required
def delete_media_entry(entry_id):
    if not is_current_user_an_admin():
        return redirect(url_for('hello_world'))
    media_service.delete_by_id(entry_id)
    flash("Opłata za media została usunięta", 'success')
    return redirect(url_for('admin.admin_media'))


@admin.route('/group')
@login_required
def admin_user_group():
    if not is_current_user_an_admin():
        return redirect(url_for('hello_world'))
    groups = group_service.get_all()
    return render_template('admin_group.html', groups=groups)


@admin.route('/group/new', methods=['GET', 'POST'])
@login_required
def admin_add_user_group():
    if not is_current_user_an_admin():
        return redirect(url_for('hello_world'))
    form = UserGroupForm()
    if form.validate_on_submit():
        group_service.create(form)
        flash('Grupa została utworzona', 'success')
        return redirect(url_for('admin.admin_user_group'))
    return render_template('create_user_group.html', form=form)


@admin.route('/group/<group_id>/delete', methods=['POST'])
@login_required
def delete_user_group(group_id):
    if not is_current_user_an_admin():
        return redirect(url_for('hello_world'))
    group_service.delete_by_id(group_id)
    flash("Grupa została usunięta", 'success')
    return redirect(url_for('admin.admin_user_group'))


@admin.route('/group/<group_id>/manage', methods=['POST', 'GET'])
@login_required
def manage_user_group(group_id):
    if not is_current_user_an_admin():
        return redirect(url_for('hello_world'))
    user_group = group_service.get_by_id(group_id)
    form = AddUsersToGroupForm()
    users = user_service.get_all()
    form.users.choices = [(u.id, u.username) for u in users]
    if form.validate_on_submit():
        user_ids = form.users.data
        group_service.update_group(user_group, user_ids)
        flash('Definicja dodana', 'success')

    return render_template('manage_ user_group.html', form=form, group_users=user_group.users)


@admin.route('/definition')
@login_required
def admin_definition():
    if not is_current_user_an_admin():
        return redirect(url_for('hello_world'))
    definitions = definition_service.get_all()
    return render_template('admin_definition.html', definitions=definitions)


@admin.route('/definition/new', methods=['GET', 'POST'])
@login_required
def admin_add_definition():
    if not is_current_user_an_admin():
        return redirect(url_for('hello_world'))
    form = DefinitionForm()
    if form.validate_on_submit():
        definition_service.create(form)
        flash('Definicja dodana', 'success')
        return redirect(url_for('admin.admin_definition'))
    return render_template('create_definition.html', form=form)


@admin.route('/definition/<definition_id>/delete', methods=['POST'])
@login_required
def delete_definition(definition_id):
    if not is_current_user_an_admin():
        return redirect(url_for('hello_world'))
    definition_service.delete_by_id(definition_id)
    flash("Definicja została usunięta", 'success')
    return redirect(url_for('admin.admin_definition'))


@admin.route('/cleaning')
@login_required
def admin_cleaning():
    if not is_current_user_an_admin():
        return redirect(url_for('hello_world'))

    return render_template('admin_cleaning.html')


@admin.route('/cleaning_shuffle', methods=['POST'])
@login_required
def admin_cleaning_shuffle():
    if not is_current_user_an_admin():
        return redirect(url_for('hello_world'))
    cleaning_service.shuffle_cleaning_schedule()
    cleaning_service.reset_rooms_cleaned_state_to_false()
    mail_service.send_to_all_active_users(constants.SHUFFLE_CLEANING_SCHED_SUBJECT, constants.SHUFFLE_CLEANING_SCHED_BODY)
    flash("Użytkownicy przydzieleni do zadań", 'success')
    return render_template('admin_cleaning.html')


@admin.route('/notifications')
@login_required
def admin_notifications():
    if not is_current_user_an_admin():
        return redirect(url_for('hello_world'))

    return render_template('admin_notifications.html')


@admin.route('/notify_internet_debtors', methods=['GET'])
@login_required
def notify_internet_debtors():
    if not is_current_user_an_admin():
        return redirect(url_for('hello_world'))
    emails = user_service.get_internet_debtors_emails()
    for email in emails:
        mail_service.send_to_user(constants.REMIND_INTERNET_DEBT_SUBJECT, constants.REMIND_INTERNET_DEBT_BODY, email)

    flash("Powiadomienia wysłane", 'success')
    return render_template('admin_notifications.html')