from flask_mail import Message

from stancjainfo import mail, app
from stancjainfo import user_service


def send(msg: Message):
    try:
        with app.app_context():
            mail.send(msg)
    except Exception as e:
        print('Exception occurred while sending an email!', e)


def send_to_all_active_users(subject: str, body: str):
    users_emails = user_service.get_all_active_users_emails()
    msg = Message(subject=subject, body=body, recipients=users_emails)
    send(msg)


def send_to_user(subject: str, body: str, recipient: str):
    msg = Message(subject=subject, body=body, recipients=[recipient])
    send(msg)
