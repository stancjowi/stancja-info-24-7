from flask import render_template, Blueprint, flash
from flask_login import login_required, current_user

from stancjainfo import locator_debts_service, user_service
from stancjainfo.forms import AddDebtForm, RemoveDebtForm

locator_debts_module = Blueprint('locator_debts_module', __name__)


@locator_debts_module.route('/locator_debts')
@login_required
def locator_debts():
    users = user_service.get_locator_debtors_dto()
    user_map = user_service.tuple_list_to_user_map(users)
    debts_table = locator_debts_service.get_debts_table(users)
    latest = locator_debts_service.get_latest()
    return render_template('locators_debt.html', debts_table=debts_table, users=users, latest=latest, user_map=user_map)


@locator_debts_module.route('/locator_debts/add', methods=['GET', 'POST'])
@login_required
def add_debt():
    users = user_service.get_locator_debtors_dto()
    other_users = [x for x in users if x[0] is not current_user.id]
    form = AddDebtForm()
    form.who.choices = other_users
    if form.validate_on_submit():
        locator_debts_service.create_lend_debt(form)
        flash('Dług dodany', 'success')
    return render_template('debt_form.html', form=form)


@locator_debts_module.route('/locator_debts/remove', methods=['GET', 'POST'])
@login_required
def remove_debt():
    users = user_service.get_locator_debtors_dto()
    other_users = [x for x in users if x[0] is not current_user.id]
    form = RemoveDebtForm()
    form.who.choices = other_users
    if form.validate_on_submit():
        locator_debts_service.create_return_debt(form)
        flash('Dług spłacony', 'success')
    return render_template('debt_form.html', form=form)
