from typing import List

from flask import flash
from wtforms import ValidationError

from stancjainfo import db
from stancjainfo.forms import MediaEntryForm, UpdateMediaEntryForm
from stancjainfo.models import MediaEntry


def get_by_id(entry_id: int) -> MediaEntry:
    return MediaEntry.query.get_or_404(entry_id)


def get_latest() -> MediaEntry:
    return MediaEntry.query.order_by(MediaEntry.created_date.desc()).first()


def get_all_sorted_by_time() -> List[MediaEntry]:
    return MediaEntry.query.order_by(MediaEntry.created_date.desc()).all()


def get_all() -> List[MediaEntry]:
    return MediaEntry.query.all()


def create_entry(form: MediaEntryForm):
    media_entry = MediaEntry(
        month=form.month.data,
        year=form.year.data,
        payment_amount=form.payment_amount.data,
        cold_water_kitchen=form.cold_water_kitchen.data,
        warm_water_kitchen=form.warm_water_kitchen.data,
        cold_water_bathroom=form.cold_water_bathroom.data,
        warm_water_bathroom=form.warm_water_bathroom.data,
        current=form.current.data,
        gas=form.gas.data,
        current_refund=form.current_refund.data,
        created_date=form.created_date.data)
    db.session.add(media_entry)
    db.session.commit()


def update_entry(media_entry: MediaEntry, form: UpdateMediaEntryForm):
    existing_entry = MediaEntry.query.filter_by(month=form.month.data, year=int(form.year.data)).first()
    if existing_entry and existing_entry is not media_entry:
        flash("Nie można edytować tego miesiąca - opłata za ten miesiąc istnieje", 'error')
        raise ValidationError('Entry for that month already exists')
    media_entry.month = form.month.data
    media_entry.year = form.year.data
    media_entry.payment_amount = form.payment_amount.data
    media_entry.cold_water_kitchen = form.cold_water_kitchen.data
    media_entry.warm_water_kitchen = form.warm_water_kitchen.data
    media_entry.cold_water_bathroom = form.cold_water_bathroom.data
    media_entry.warm_water_bathroom = form.warm_water_bathroom.data
    media_entry.current = form.current.data
    media_entry.gas = form.gas.data
    media_entry.current_refund = form.current_refund.data
    media_entry.created_date = form.created_date.data
    db.session.commit()


def fill_update_form(media_entry: MediaEntry, form: UpdateMediaEntryForm) -> UpdateMediaEntryForm:
    form.month.data = media_entry.month.name
    form.year.data = int(media_entry.year)
    form.payment_amount.data = media_entry.payment_amount
    form.cold_water_kitchen.data = media_entry.cold_water_kitchen
    form.warm_water_kitchen.data = media_entry.warm_water_kitchen
    form.cold_water_bathroom.data = media_entry.cold_water_bathroom
    form.warm_water_bathroom.data = media_entry.warm_water_bathroom
    form.current.data = media_entry.current
    form.gas.data = media_entry.gas
    form.current_refund.data = media_entry.current_refund
    form.created_date.data = media_entry.created_date
    return form


def delete_by_id(entry_id: int):
    media_entry = MediaEntry.query.get_or_404(entry_id)
    db.session.delete(media_entry)
    db.session.commit()