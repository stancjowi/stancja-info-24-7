import os

from flask import Flask
from flask_avatars import Avatars
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_mail import Mail
from flask_socketio import SocketIO
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

# TODO env var
app.config['SECRET_KEY'] = '34da334d734b00009983b65f33e48af7'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db'
app.config['AVATARS_SAVE_PATH'] = os.path.join(app.root_path, 'static', 'img', 'generated')

# mail
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
app.config['MAIL_USERNAME'] = 'stancjainfo24@gmail.com'
app.config['MAIL_PASSWORD'] = 'eykxmvshmwvhqiul'
app.config['MAIL_DEFAULT_SENDER'] = 'stancjainfo24@gmail.com'

mail = Mail(app)

db = SQLAlchemy(app)
db.create_all()
db.session.commit()

bcrypt = Bcrypt(app)

socketio = SocketIO(app)

avatars = Avatars(app)

login_manager = LoginManager(app)
login_manager.login_view = 'login'
login_manager.login_message_category = 'light'
login_manager.login_message = 'Zaloguj się aby zobaczyć zawartość strony'

from stancjainfo import routes

from stancjainfo.admin_routes import admin
from stancjainfo.internet_routes import internet_module
from stancjainfo.media_routes import media_module
from stancjainfo.locator_debts_routes import locator_debts_module
from stancjainfo.weather_routes import weather_module
from stancjainfo.cleaning_routes import cleaning_module
from stancjainfo.communication_routes import communication_module
from stancjainfo.profile_routes import profile_module

app.register_blueprint(admin)
app.register_blueprint(internet_module)
app.register_blueprint(media_module)
app.register_blueprint(locator_debts_module)
app.register_blueprint(weather_module)
app.register_blueprint(cleaning_module)
app.register_blueprint(communication_module)
app.register_blueprint(profile_module)

# TODO: warunek
import stancjainfo.scheduler_service as scheduler_service

scheduler_service.init_scheduler_test()

