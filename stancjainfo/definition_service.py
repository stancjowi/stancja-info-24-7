from typing import List

from stancjainfo import db
from stancjainfo.forms import DefinitionForm
from stancjainfo.models import Definition

__debtors_key = 'debtors'

__locator_debts_key = 'locator_debts'

__media_payers_key = 'media_payers'

__cleaner_kitchen_worktops = 'cleaner_kitchen_worktops'  # blaty i kuchenka gazowa
__cleaner_floors = 'cleaner_floors'  # odkurzanie i mycie
__cleaner_bathroom = 'cleaner_bathroom'
__cleaner_kitchen_other = 'cleaner_kitchen_other'  # sciany, piekarnik, mikrofala, zlew, okap
__cleaner_restroom = 'cleaner_restroom'  # kibel i śmieci

__last_cleaning_schedule_set_date = 'last_cleaning_sched_date'

__is_kitchen_worktops_cleaned = 'is_kitchen_worktops_cleaned'
__is_floors_cleaned = 'is_floors_cleaned'
__is_bathroom_cleaned = 'is_bathroom_cleaned'
__is_kitchen_other_cleaned = 'is_kitchen_other_cleaned'
__is_restroom_cleaned = 'is_restroom_cleaned'


def get_all() -> List[Definition]:
    return Definition.query.all()


def delete_by_id(definition_id: int):
    definition = Definition.query.get_or_404(definition_id)
    db.session.delete(definition)
    db.session.commit()


def create(form: DefinitionForm):
    definition = Definition(
        key=form.key.data,
        value=form.value.data
    )
    db.session.add(definition)
    db.session.commit()


def get_by_id(definition_id: int) -> Definition:
    return Definition.query.get_or_404(definition_id)


def get_by_key(key: str) -> Definition:
    return Definition.query.filter_by(key=key).first()


def update_definition(definition: Definition, value: str):
    definition.value = value
    db.session.commit()