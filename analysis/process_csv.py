#!/usr/bin/python

import csv
import datetime

formatter = '%Y-%m-%d %H:%M:%S.%f'
timeObj = datetime.datetime.strptime('2019-10-03 12:31:37.708743', formatter)

print('Date:', timeObj.date())
print('Time:', timeObj.time())
print('Date-time:', timeObj)


class Entry:

    def __init__(self, id: int, pm25: float, pm10: float, timestamp: datetime):
        self.id = id
        self.pm25 = pm25
        self.pm10 = pm10
        self.timestamp = timestamp


def main():
    smog_entries = []
    start_timestamp = 1570043017
    end_timestamp = 1586530254
    with open('./smog.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        for row in csv_reader:
            date = datetime.datetime.strptime(row[3], formatter)
            timestamp = datetime.datetime.timestamp(date)
            smog_entries.append(
                Entry(int(row[0]), float(row[1]), float(row[2]), timestamp)
            )
            print(timestamp)

if __name__ == '__main__':
    main()
